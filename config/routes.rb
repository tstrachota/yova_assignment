# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :admin do
    resources :notifications, only: %i[index create]
  end

  resources :notifications, only: %i[index]
  get "/portfolio" => "clients#show_portfolio"
end
