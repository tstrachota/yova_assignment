# frozen_string_literal: true

FactoryBot.define do
  factory :administrator do
    sequence(:login) { |n| "admin_#{n}" }
    sequence(:password) { |n| "admin_#{n}" }
  end

  factory :company do
    symbol { SecureRandom.alphanumeric(4).upcase }
  end

  factory :client do
    sequence(:name) { |n| "Client #{n}" }
    sequence(:login) { |n| "client_#{n}" }
    sequence(:password) { |n| "client_#{n}" }

    trait :with_companies do
      companies { build_list(:company, 40) }
    end
  end

  factory :notification do
    text { "Lorem Ipsum" }
  end

  factory :client_notification do
    notification { create(:notification) }
    read_at { Time.zone.now }

    trait :unread do
      read_at { nil }
    end
  end
end
