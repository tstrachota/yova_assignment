# frozen_string_literal: true

require "rails_helper"

RSpec.describe Client, type: :model do
  describe "#unread_notifications" do
    it "returns only unread unread" do
      read_notifications = build_list(:client_notification, 3)
      unread_notifications = build_list(:client_notification, 2, :unread)
      client = create(:client, client_notifications: read_notifications + unread_notifications)

      expect(client.unread_notifications).to eq(unread_notifications.map(&:notification))
    end
  end
end
