# frozen_string_literal: true

require "rails_helper"

RSpec.describe ReadClientNotifications do
  let(:read_notifications) { build_list(:client_notification, 3) }
  let(:unread_notifications) { build_list(:client_notification, 2, :unread) }
  let(:client) { create(:client, client_notifications: read_notifications + unread_notifications) }

  it "returns client's unread notifications" do
    notifications = ReadClientNotifications.call(client)
    expect(notifications).to eq(unread_notifications.map(&:notification))
  end

  it "marks the notifications as read" do
    expect(client.unread_notifications).not_to be_empty
    ReadClientNotifications.call(client)
    expect(client.unread_notifications).to be_empty
  end
end
