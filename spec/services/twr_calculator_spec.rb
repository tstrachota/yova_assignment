# frozen_string_literal: true

require "rails_helper"

RSpec.describe TwrCalculator do
  let(:read_notifications) { build_list(:client_notification, 3) }
  let(:unread_notifications) { build_list(:client_notification, 2, :unread) }
  let(:client) { create(:client, client_notifications: read_notifications + unread_notifications) }

  it "calculates 0 TWR when there's no change in values" do
    twr = TwrCalculator.call([1.0, 1.0, 1.0])
    expect(twr).to eq(0)
  end

  it "calculates positive TWR" do
    twr = TwrCalculator.call([1, 2, 2, 3, 3])
    expect(twr).to eq(2)
  end

  it "calculates negative TWR" do
    twr = TwrCalculator.call([5.6, 2.8, 2.1])
    expect(twr).to eq(-0.625)
  end
end
