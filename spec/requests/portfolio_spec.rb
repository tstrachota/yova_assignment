# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Portfolio API", type: :request do
  describe "GET /portfolio" do
    let(:client) { create(:client, :with_companies) }

    it "returns portfolio data" do
      get "/portfolio", headers: http_login(client)

      expect(response_data[:twr]).not_to be_nil
      expect(response_data[:companies].count).to eq(client.companies.count)

      expect(response).to have_http_status(200)
    end

    it "requires authentication" do
      get "/portfolio"
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
