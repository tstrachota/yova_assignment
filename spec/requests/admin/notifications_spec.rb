# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Admin Notifications API", type: :request do
  let(:admin) { create(:administrator) }

  describe "GET /admin/notifications" do
    before { create_list(:notification, 3) }

    it "returns notifications" do
      get "/admin/notifications", headers: http_login(admin)

      expect(response_data).not_to be_empty
      expect(response_data.size).to eq(3)

      expect(response).to have_http_status(200)
    end

    it "requires authentication" do
      get "/admin/notifications"
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe "POST /admin/notifications" do
    context "with valid params" do
      let(:clients) { create_list(:client, 2) }
      let(:client_ids) { clients.map(&:id) }
      let(:valid_attrs) { { text: "Some message", client_ids: client_ids } }

      it "creates a notification" do
        post "/admin/notifications", params: { notification: valid_attrs }, headers: http_login(admin)

        expect(response_data[:id]).not_to be_nil
        expect(response_data[:text]).to eq("Some message")
        expect(response_data[:clients].map { |c| c[:id] }).to eq(client_ids)

        expect(response).to have_http_status(200)
      end
    end

    it "requires a notification param" do
      post "/admin/notifications", params: {}, headers: http_login(admin)
      expect(response_error).to match("empty: notification")
    end

    it "requires authentication" do
      get "/admin/notifications"
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
