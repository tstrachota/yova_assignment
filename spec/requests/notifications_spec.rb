# frozen_string_literal: true

require "rails_helper"

RSpec.describe "Notifications API", type: :request do
  describe "GET /notifications" do
    let(:notifications) { build_list(:notification, 3) }
    let(:client) { create(:client) }

    it "returns unread notifications" do
      expect(ReadClientNotifications).to receive(:call) { notifications }

      get "/notifications", headers: http_login(client)

      expect(response_data).not_to be_empty
      expect(response_data.size).to eq(3)

      expect(response).to have_http_status(200)
    end

    it "requires authentication" do
      get "/notifications"
      expect(response).to have_http_status(:unauthorized)
    end
  end
end
