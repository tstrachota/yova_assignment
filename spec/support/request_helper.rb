# frozen_string_literal: true

module RequestHelper
  def response_json
    JSON.parse(response.body).with_indifferent_access
  end

  def response_data
    response_json[:data]
  end

  def response_error
    response_json[:error]
  end

  def http_login(user)
    # Test users use logins as their passwords
    login = user.login
    password = user.login
    {
      "HTTP_AUTHORIZATION" => ActionController::HttpAuthentication::Basic.encode_credentials(login, password)
    }
  end
end
