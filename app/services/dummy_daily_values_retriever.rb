# frozen_string_literal: true

class DummyDailyValuesRetriever
  # Number of days to retrieve returns for
  HISTORY_LIMIT_IN_DAYS = 30

  attr_reader :company

  def self.call(company)
    new(company).call
  end

  def initialize(company)
    @company = company
  end

  def call
    # This deliberately returns the limit+1 record
    daily_values[0..HISTORY_LIMIT_IN_DAYS]
  end

  private

  def daily_values
    @daily_values ||= begin
      # Values are sorted from newer to older
      json_data["Time Series (Daily)"].values.reverse.map { |v| v["4. close"].to_f }
    end
  end

  def json_data
    JSON.parse(File.read(data_file_path))
  end

  def data_file_path
    # Select a file with dummy data according to ID of the company
    file_index = company.persisted? ? company.id % available_data_files.count : 0
    available_data_files[file_index]
  end

  def available_data_files
    @available_data_files = Dir[Rails.root.join("data/daily_values/*.json")].sort
  end
end
