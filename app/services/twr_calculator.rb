# frozen_string_literal: true

class TwrCalculator
  attr_reader :daily_values

  def self.call(daily_values)
    new(daily_values).call
  end

  def initialize(daily_values)
    @daily_values = daily_values
  end

  def call
    daily_returns_plus_one.reduce(:*) - 1
  end

  private

  def daily_returns_plus_one
    (daily_values.count - 1).times.map { |idx| daily_values[idx + 1].to_f / daily_values[idx] }
  end
end
