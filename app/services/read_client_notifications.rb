# frozen_string_literal: true

class ReadClientNotifications
  attr_reader :client

  def self.call(client)
    new(client).call
  end

  def initialize(client)
    @client = client
  end

  def call
    # Get notifications and mark them as read
    notifications = client.unread_notifications.load
    client.client_notifications.where(read_at: nil).update(read_at: Time.zone.now)
    notifications
  end
end
