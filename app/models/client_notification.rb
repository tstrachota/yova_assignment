# frozen_string_literal: true

class ClientNotification < ApplicationRecord
  belongs_to :client
  belongs_to :notification
end
