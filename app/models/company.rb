# frozen_string_literal: true

class Company < ApplicationRecord
  belongs_to :client

  def self.new_with_random_symbol
    new(symbol: SecureRandom.alphanumeric(4).upcase)
  end
end
