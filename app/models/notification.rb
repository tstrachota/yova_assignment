# frozen_string_literal: true

class Notification < ApplicationRecord
  has_many :client_notifications, dependent: :destroy
  has_many :clients, through: :client_notifications
end
