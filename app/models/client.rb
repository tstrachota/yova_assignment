# frozen_string_literal: true

class Client < ApplicationRecord
  has_many :client_notifications, dependent: :destroy
  has_many :notifications, through: :client_notifications
  has_many :companies, dependent: :destroy

  has_secure_password

  def unread_notifications
    notifications.joins(:client_notifications).where("client_notifications.read_at": nil)
  end
end
