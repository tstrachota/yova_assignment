# frozen_string_literal: true

class Seeder
  CLIENT_NAMES = %w[Alpha Beta Gamma Delta].freeze
  CLIENT_COMPANY_COUNT = 40
  ADMIN_LOGIN = "admin"

  def self.seed_admin
    Administrator.create!(login: ADMIN_LOGIN, password: ADMIN_LOGIN) unless Administrator.find_by(login: ADMIN_LOGIN)
  end

  def self.seed_clients
    CLIENT_NAMES.each do |client_name|
      login = client_name.downcase
      client = Client.find_by(name: client_name)
      client = Client.create!(name: client_name, login: login, password: login) if client.nil?
      CLIENT_COMPANY_COUNT.times { client.companies << Company.new_with_random_symbol } if client.companies.empty?
    end
  end
end
