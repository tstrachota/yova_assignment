# frozen_string_literal: true

# require_dependency "presenter"

module Admin
  class NotificationPresenter < SimpleDelegator
    def self.map(notifications)
      notifications.map { |n| new(n) }
    end

    def as_json(attrs)
      super(attrs).merge({
        clients: clients_with_status
      })
    end

    private

    def clients_with_status
      client_notifications.includes(:client).all.map do |client_notification|
        {
          id: client_notification.client.id,
          name: client_notification.client.name,
          read: !client_notification.read_at.nil?
        }
      end
    end
  end
end
