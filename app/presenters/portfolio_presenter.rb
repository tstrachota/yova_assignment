# frozen_string_literal: true

class PortfolioPresenter
  attr_reader :client

  def initialize(client)
    @client = client
    @company_twrs = {}
  end

  def as_json(*)
    {
      twr: monthly_twr,
      companies: companies
    }
  end

  private

  def companies
    client.companies.map do |c|
      {
        symbol: c.symbol,
        twr: company_twr(c)
      }
    end
  end

  def monthly_twr
    client.companies.map { |company| company_twr(company) }.sum
  end

  def company_twr(company)
    @company_twrs[company.symbol] ||= begin
      weight = 1.0 / client.companies.count
      weight * TwrCalculator.call(DummyDailyValuesRetriever.call(company))
    end
  end
end
