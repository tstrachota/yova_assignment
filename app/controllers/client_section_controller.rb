# frozen_string_literal: true

class ClientSectionController < ApplicationController
  include ActionController::HttpAuthentication::Basic::ControllerMethods

  before_action :authenticate

  attr_reader :current_client

  def authenticate
    authenticate_or_request_with_http_basic do |login, password|
      client = Client.find_by(login: login)
      @current_client = client if client&.authenticate(password)
    end
  end
end
