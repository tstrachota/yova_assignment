# frozen_string_literal: true

class NotificationsController < ClientSectionController
  def index
    notifications = Admin::NotificationPresenter.map(ReadClientNotifications.call(current_client))
    render_data(notifications)
  end
end
