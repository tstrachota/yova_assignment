# frozen_string_literal: true

class ClientsController < ClientSectionController
  def show_portfolio
    render_data(PortfolioPresenter.new(current_client))
  end
end
