# frozen_string_literal: true

class ApplicationController < ActionController::API
  rescue_from ActiveRecord::ActiveRecordError, with: :render_bad_request
  rescue_from ActionController::ParameterMissing, with: :render_bad_request

  private

  def render_data(data)
    render json: { data: data }
  end

  def render_error(status_code, error_data)
    render json: { error: error_data }, status: status_code
  end

  def render_bad_request(exception)
    render_error(400, exception.message)
  end
end
