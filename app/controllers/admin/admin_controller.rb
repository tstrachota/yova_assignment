# frozen_string_literal: true

module Admin
  class AdminController < ApplicationController
    include ActionController::HttpAuthentication::Basic::ControllerMethods

    before_action :authenticate

    attr_reader :current_admin

    def authenticate
      authenticate_or_request_with_http_basic do |login, password|
        admin = Administrator.find_by(login: login)
        @current_admin = admin if admin&.authenticate(password)
      end
    end
  end
end
