# frozen_string_literal: true

module Admin
  class NotificationsController < AdminController
    def index
      # TODO: pagination
      notifications = Admin::NotificationPresenter.map(Notification.all)
      render_data(notifications)
    end

    def create
      notification = Notification.create!(notification_params)
      render_data(Admin::NotificationPresenter.new(notification))
    end

    private

    def notification_params
      params.require(:notification).permit(:text, client_ids: [])
    end
  end
end
