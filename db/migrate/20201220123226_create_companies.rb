# frozen_string_literal: true

class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.integer :client_id, null: false
      t.string :symbol

      t.timestamps
    end
  end
end
