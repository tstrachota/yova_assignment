# frozen_string_literal: true

class AddUniqueIndexToClientsLogin < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def change
    add_index :clients, :login, unique: true, algorithm: :concurrently
  end
end
