# frozen_string_literal: true

class CreateClientNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :client_notifications do |t|
      t.integer :client_id, null: false
      t.integer :notification_id, null: false
      t.timestamp :read_at

      t.timestamps
    end
  end
end
