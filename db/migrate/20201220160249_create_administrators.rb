# frozen_string_literal: true

class CreateAdministrators < ActiveRecord::Migration[5.2]
  def change
    create_table :administrators do |t|
      t.string :login
      t.string :password_digest

      t.timestamps
    end
    add_index :administrators, :login, unique: true
  end
end
