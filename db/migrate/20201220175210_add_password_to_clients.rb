# frozen_string_literal: true

class AddPasswordToClients < ActiveRecord::Migration[5.2]
  def change
    add_column :clients, :password_digest, :string
  end
end
