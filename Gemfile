# frozen_string_literal: true

source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "2.7.2"

gem "bcrypt", "~> 3.1"
gem "bootsnap", ">= 1.1.0", require: false
gem "factory_bot_rails", "~> 6.1"
gem "jbuilder", "~> 2.5"
gem "pg", ">= 0.18", "< 2.0"
gem "puma", "~> 3.11"
gem "rails", "~> 5.2.4", ">= 5.2.4.4"
gem "strong_migrations", "~> 0.7.4"

group :development, :test do
  gem "pry-byebug"
  gem "pry-rails"
  gem "rspec-rails", "~> 4.0"
  gem "rubocop", "~> 1.6"
  gem "rubocop-rails", "~> 2.9"
end

group :development do
  gem "listen", ">= 3.0.5", "< 3.2"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[mingw mswin x64_mingw jruby]
